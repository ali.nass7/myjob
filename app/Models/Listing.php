<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'title',
        'description',
        'roles',
        'job_type',
        'address',
        'salary',
        'feature_image',
        'application_close_date',
        'slug'
    ];

    // public function getPathAttribute($value){
    //     return asset('storage/'.$value);
    // }
    public function users(){
        return $this->belongsToMany(User::class,'listing_user','listing_id','user_id')->withPivot('shortlisted')->withTimestamps();

    }

    public function profile(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}
