<?php


namespace App\Post;

use App\Models\Listing;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class JobPost{

    protected $listing;
    public function __construct(Listing $listing)
    {
        $this->listing= $listing;
    }

    public function getImagePath(Request $request)
    {
        return $request->file('feature_image')->store('images','public');
    }

    public function store(Request $request):void
    {
        // dd($request);
        // $imagePath= $request->file('feature_image')->store('images','public');
        $imagePath= $this->getImagePath($request);
        $this->listing->feature_image=$imagePath;
        $this->listing->user_id=auth()->user()->id;
        $this->listing->title=$request->title;
        $this->listing->description=$request->description;
        $this->listing->roles=$request->roles;
        $this->listing->job_type=$request->job_type;
        $this->listing->address=$request->address;
        $this->listing->application_close_date=\Carbon\Carbon::createFromFormat('d/m/Y',$request->date)->format('Y-m-d');
        $this->listing->salary=$request->salary;
        $this->listing->slug=Str::slug($request->title).'.'.Str::uuid();
        $this->listing->save();
    }

    public function updatePost(int $id,Request $request){
        if($request->hasFile('feature_image'))
        {
            // $imagePath= $request->file('feature_image')->store('images','public');
            $this->listing->findOrFail($id)->update([
                'feature_image'=>$this->getImagePath($request)
            ]);
        }
        $this->listing->findOrFail($id)->update($request->except('feature_image'));
        
    }
}