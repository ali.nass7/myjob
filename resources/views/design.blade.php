@extends('layouts.app')
@section('content')
<div class="container">
    <h1>My Dribble designs</h1>
    <div class="row">
        @foreach ($designs as $design )
            <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                    <img class="card-img-top">
                    <div class="card-body">
                        <p class="card-text"> {{$design['title']}} </p>
                        <div class="d-flex">
                            
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection