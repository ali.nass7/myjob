<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Events\Registere;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\SeekerRegistrationRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function createSeeker(){
        return view('user.seeker-register');
    }
    public function createEmployer(){
        return view('user.employer-register');
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'current_password'=>'required',
            'password'=>'required|min:6|confirmed'
        ]);
        $user=auth()->user();
        if(!Hash::check($request->current_password,$user->password))
        {
            return back()->with('error','Current password is incorrect');
        }
        $user->password= Hash::make($request->password);
        $user->save();

        return back()->with('success','Your password has been updated successfully');
    }


    public function storeSeeker( SeekerRegistrationRequest $request ){
        
        $user=User::create([
            'name'=>request('name'),
            'email'=>request('email'),
            'password'=>bcrypt(request('password')),
            'user_type'=>'seeker'
        ]);
        Auth::login($user);
        $user->sendEmailVerificationNotification();
        return response()->json('success');
        // return redirect()->route('login')->with('successMessage','Your account was created');    
    }
    
    public function storeEmployer(){
        $user=User::create([
            'name'=>request('name'),
            'email'=>request('email'),
            'password'=>bcrypt(request('password')),
            'user_type'=>'employer',
            'user_trial'=>now()->addWeek(),
        ]);
        Auth::login($user);
        $user->sendEmailVerificationNotification();
        return response()->json('success');
        // return redirect()->route('login')->with('successMessage','Your account was created');    
    }
    
    public function login(){
        return view('user.login');
    }

    public function postLogin(Request $request){
        $request->validate([
            'email'=>'required',
            'password'=>'required',
        ]);

        $credentails= $request->only('email','password');
        
        if(Auth::attempt($credentails)){
            if(auth()->user()->user_type=='employer')
            {
                return redirect()->to('dashboard');
            }else{
                return redirect()->to('/');
            }
        }
        return back();
    }

    public function logout(){
        auth()->logout();
        return redirect()->route('login');
    }
    
    public function profile()
    {
        return view('profile.index');
    }
    
    public function update(Request $request)
    {
        if($request->hasFile('profile_pic'))
        {
            $imagePath= $request->file('profile_pic')->store('images','public');
            // dd($imagePath);
           $user= User::findOrFail(auth()->user()->id)->update(['profile_pic'=>$imagePath]);
            dd($user);
        }
        User::findOrFail(auth()->user()->id)->update($request->except('feature_image'));

        return back()->with('success','Your profile has been updated!');
    }

    public function seekerProfile(){
        return view('seeker.profile');
    }

    public function uploadResume(Request $request){
        $this->validate($request,[
            'resume'=>'required|mimes:pdf,doc,docx'
        ]);
        if($request->hasFile('resume'))
        {
            $resume= $request->file('resume')->store('resume','public');
            User::find(auth()->user()->id)->update(['resume'=>$resume]);
            return back()->with('success','Your resume has been updated');
        }
        return back()->with('error','please try again');
    }

    public function jobApplied(){
        $users= User::with('listings')->where('id',auth()->user()->id)->get();
        return view('seeker.job-applied',compact('users'));
    }
}
