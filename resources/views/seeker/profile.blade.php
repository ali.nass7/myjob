@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            @if(Session::has('success'))
                <div class="alert-success alert">
                    {{Session::get('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert-danger alert">
                    {{Session::get('error')}}
                </div>
            @endif
            <h2> Update Your Profile</h2>    
            <form action="{{route('user.update.profile')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="name">Profile image</label>
                        <input type="file" class="form-control" name="profile_pic" id="logo">
                        @if (auth()->user()->profile_pic)
                         <img src="{{asset('storage/'.auth()->user()->profile_pic)}}" width="150px" alt="s" class="mt-3">
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="name">Your name</label>
                        <input type="text" name="name" class="form-control" id="" value="{{auth()->user()->name}}">
                    </div>
                    <div class="form-group mt-4">
                        <button type="submit"  class="btn btn-success">Update</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="row justify-content-center">
            <h3>Change Your Password</h3>
            <form action="{{route('user.password')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="name">Profile image</label>
                    </div>
                    <div class="form-group">
                        <label for="name">Your Current Password</label>
                        <input type="password" name="current_password" class="form-control" id="" value="{{auth()->user()->name}}">
                    </div>
                    <div class="form-group">
                        <label for="name">Your New Password</label>
                        <input type="password" name="password" class="form-control" id="" value="{{auth()->user()->name}}">
                    </div>
                    <div class="form-group">
                        <label for="name">Confirm Password</label>
                        <input type="password" name="password_confirmation" class="form-control" id="" value="{{auth()->user()->name}}">
                    </div>
                    <div class="form-group mt-4">
                        <button type="submit"  class="btn btn-success">Update</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="row justify-content-center">
            <h3>Update Your Resume</h3>
            <form action="{{route('upload.resume')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="resume">Upload a resume</label>
                        <input type="file" name="resume" class="form-control" id="resume" value="{{auth()->user()->name}}">
                    </div>
                    <div class="form-group mt-4">
                        <button type="submit"  class="btn btn-success">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection